import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateOrderDto, UpdateOrderDto } from 'src/users/dtos/order.dto';

import { Order } from 'src/users/entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(@InjectModel(Order.name) private orderModel: Model<Order>) {}

  findAll() {
    return this.orderModel
      .find()
      .populate('customer')
      .populate({ path: 'products', model: 'Product' })
      .exec();
  }

  async findOne(id: string) {
    const order = await this.orderModel
      .findById(id)
      .populate('customer')
      .populate({
        path: 'products',
        model: 'Product',
      })
      .exec();
    if (!order) {
      throw new NotFoundException(`Order #${id} not found`);
    }
    return order;
  }

  create(data: CreateOrderDto) {
    const newOrder = new this.orderModel(data);
    return newOrder.save();
  }

  update(id: string, payload: UpdateOrderDto) {
    const order = this.orderModel.findByIdAndUpdate(
      id,
      { $set: payload },
      { new: true },
    );
    if (!order) {
      throw new NotFoundException(`Order #${id} not found`);
    }
    return order;
  }

  remove(id: string) {
    return this.orderModel.findByIdAndDelete(id);
  }

  async addProduct(id: string, productId: string) {
    const order = await this.orderModel.findById(id);
    if (!order) {
      throw new NotFoundException(`Order #${id} not found`);
    }
    order.products.push(productId);
    return order.save();
  }

  async removeProdcut(id: string, productId: string) {
    const order = await this.orderModel.findById(id);
    if (!order) {
      throw new NotFoundException(`Order #${id} not found`);
    }
    order.products.pull(productId);
    return order.save();
  }
}
