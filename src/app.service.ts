import { Inject, Injectable } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { Db } from 'mongodb';

import config from './config';

@Injectable()
export class AppService {
  constructor(
    @Inject('TASKS') private tasks: any[],
    // private configService: ConfigService,
    @Inject(config.KEY) private configService: ConfigType<typeof config>,
    @Inject('MONGO') private database: Db,
  ) {}

  getHello(): string {
    // console.log(this.tasks);
    const apiKey = this.configService.apiKey;
    const database = this.configService.database.name;

    return `Hello World! ${apiKey} ${database}`;
  }

  getTasks() {
    const taskCollection = this.database.collection('tasks');
    return taskCollection.find().toArray();
  }
}
